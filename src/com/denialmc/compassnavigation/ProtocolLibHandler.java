package com.denialmc.compassnavigation;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.Packets;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import com.comphenix.protocol.wrappers.nbt.NbtFactory;

public class ProtocolLibHandler {
	
	public ProtocolLibHandler(CompassNavigation plugin) {
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(plugin, ConnectionSide.SERVER_SIDE, ListenerPriority.NORMAL, Packets.Server.SET_SLOT, Packets.Server.WINDOW_ITEMS) {
			public void onPacketSending(PacketEvent event) {
				if (event.getPacketType() == PacketType.Play.Server.SET_SLOT) {
					modifyItem(new ItemStack[] { event.getPacket().getItemModifier().read(0) });
				} else {
					modifyItem(event.getPacket().getItemArrayModifier().read(0));
				}
	        }
		});
	}
	
	public void modifyItem(ItemStack[] stacks) {
		for (ItemStack stack : stacks) {
			if (stack != null) {
				NbtCompound compound = (NbtCompound) NbtFactory.fromItemTag(stack);
				compound.put(NbtFactory.ofList("AttributeModifiers"));
				if (stack.getEnchantmentLevel(Enchantment.WATER_WORKER) == 4) {
					compound.put(NbtFactory.ofList("ench"));
				}
			}
		}
	}
}