package com.denialmc.compassnavigation;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class AutoUpdater {

	public CompassNavigation plugin;
	public boolean versionCheck;

	public String versionName;
	public String versionLink;
	public String versionType;
	public String versionGameVersion;
	public String updateFolder;

	public URL url;
	public File file;
	public Thread thread;

	public UpdateResult result = UpdateResult.SUCCESS;

	public String delimiter = "^v|[\\s_-]v";
	
	public enum UpdateResult {
		SUCCESS, NO_UPDATE, FAIL_DOWNLOAD, FAIL_DBO, FAIL_NOVERSION, FAIL_BADID, UPDATE_AVAILABLE
	}
	
	public enum ReleaseType {
		ALPHA, BETA, RELEASE, UNKNOWN
	}

	public AutoUpdater(CompassNavigation plugin, File file, boolean versionCheck) {
		this.plugin = plugin;
		this.file = file;
		this.versionCheck = versionCheck;
		updateFolder = plugin.getServer().getUpdateFolder();

		try {
			url = new URL("https://api.curseforge.com/servermods/files?projectIds=54751");
		} catch (Exception e) {
			e.printStackTrace();
			result = UpdateResult.FAIL_BADID;
		}

		thread = new Thread(new UpdateRunnable());
		thread.start();
	}

	public UpdateResult getResult() {
		waitForThread();
		return result;
	}

	public ReleaseType getLatestType() {
		waitForThread();
		if (versionType != null) {
			for (ReleaseType type : ReleaseType.values()) {
				if (versionType.equals(type.name().toLowerCase())) {
					return type;
				}
			}
		}
		return ReleaseType.UNKNOWN;
	}

	public String getLatestGameVersion() {
		waitForThread();
		return versionGameVersion;
	}

	public String getLatestName() {
		waitForThread();
		return versionName;
	}

	public String getLatestFileLink() {
		waitForThread();
		return versionLink;
	}

	public void download() {
		versionCheck = false;
		thread = new Thread(new UpdateRunnable());
		thread.start();
		waitForThread();
		versionCheck = true;
	}

	public void waitForThread() {
		if (thread != null && thread.isAlive()) {
			try {
				thread.join();
			} catch (Exception e) {
				plugin.getLogger().warning("Problem encountered while waiting for the update task: " + e.getMessage());
			}
		}
	}

	public void saveFile(File folder, String file, String link) {
		if (!folder.exists()) {
			folder.mkdir();
		}

		try {
			URL url = new URL(link);
			int length = url.openConnection().getContentLength();
			byte[] data = new byte[1024];
			long downloaded = 0;
			int count;

			BufferedInputStream input = new BufferedInputStream(url.openStream());
			FileOutputStream output = new FileOutputStream(folder.getAbsolutePath() + File.separator + file);

			plugin.getLogger().info("About to download a new update: " + versionName);

			while ((count = input.read(data, 0, 1024)) != -1) {
				downloaded += count;
				output.write(data, 0, count);
				int percent = (int) ((downloaded * 100) / length);
				if (percent % 10 == 0) {
					plugin.getLogger().info("Downloading update: " + percent + "% of " + length + " bytes.");
				}
			}

			plugin.getLogger().info("Finished updating.");
			result = UpdateResult.SUCCESS;

			if (input != null) {
				input.close();
			}

			if (output != null) {
				output.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = AutoUpdater.UpdateResult.FAIL_DOWNLOAD;
		}
	}

	public boolean versionCheck(String title) {
		if (versionCheck) {
			String localVersion = plugin.getDescription().getVersion();
			if (title.split(delimiter).length == 2) {
				String remoteVersion = title.split(delimiter)[1].split(" ")[0];

				if (shouldUpdate(localVersion, remoteVersion)) {
					result = AutoUpdater.UpdateResult.NO_UPDATE;
					return false;
				}
			} else {
				result = AutoUpdater.UpdateResult.FAIL_NOVERSION;
				return false;
			}
		}
		return true;
	}
	
	public boolean shouldUpdate(String localVersion, String remoteVersion) {
		return !localVersion.equalsIgnoreCase(remoteVersion);
	}

	public boolean read() {
		try {
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(5000);
			connection.setDoOutput(true);

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String response = reader.readLine();

			JSONArray array = (JSONArray) JSONValue.parse(response);

			if (array.size() == 0) {
				result = UpdateResult.FAIL_BADID;
				return false;
			}

			versionName = (String) ((JSONObject) array.get(array.size() - 1)).get("name");
			versionLink = (String) ((JSONObject) array.get(array.size() - 1)).get("downloadUrl");
			versionType = (String) ((JSONObject) array.get(array.size() - 1)).get("releaseType");
			versionGameVersion = (String) ((JSONObject) array.get(array.size() - 1)).get("gameVersion");

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			result = UpdateResult.FAIL_DBO;
			return false;
		}
	}

	public class UpdateRunnable implements Runnable {

		public void run() {
			if (url != null) {
				if (read()) {
					if (versionCheck(versionName)) {
						if (versionLink != null) {
							String name = file.getName();
							saveFile(new File(plugin.getDataFolder().getParent(), updateFolder), name, versionLink);
						} else {
							result = UpdateResult.UPDATE_AVAILABLE;
						}
					}
				}
			}
		}
	}
}